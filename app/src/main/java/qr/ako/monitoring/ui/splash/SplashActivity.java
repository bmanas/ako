package qr.ako.monitoring.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import qr.ako.monitoring.MainActivity;
import qr.ako.monitoring.R;
import qr.ako.monitoring.data.prefs.SharedPrefsHelper;

public class SplashActivity extends AppCompatActivity implements SplashView{

    private SplashPresenter presenter;
    private Disposable check;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        SharedPrefsHelper prefsHelper = new SharedPrefsHelper(this);
        presenter = new SplashPresenter(this, prefsHelper);

        check = Flowable.timer(2500, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(t -> presenter.checkMyToken())
                .subscribe(t -> presenter.goToNextScreen());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        check.dispose();
    }

    @Override
    public void goToMainActivity(int type) {
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        intent.putExtra(MainActivity.USER_TYPE, type);
        startActivity(intent);
        finish();
    }

    @Override
    public void success() {

    }

    @Override
    public void failed(String err) {
        Toast.makeText(this,err,Toast.LENGTH_LONG).show();
    }

    @Override
    public void error() {

    }
}
