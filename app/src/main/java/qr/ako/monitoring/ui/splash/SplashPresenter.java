package qr.ako.monitoring.ui.splash;

import android.util.Log;

import org.reactivestreams.Subscription;

import io.reactivex.Flowable;
import io.reactivex.FlowableSubscriber;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import qr.ako.monitoring.data.network.NetworkClient;
import qr.ako.monitoring.data.network.NetworkInterface;
import qr.ako.monitoring.data.network.models.LoginResponse;
import qr.ako.monitoring.data.prefs.SharedPrefsHelper;

public class SplashPresenter {

    private SplashView view;
    private SharedPrefsHelper prefsHelper;

    SplashPresenter(SplashView view, SharedPrefsHelper prefsHelper) {
        this.view = view;
        this.prefsHelper = prefsHelper;
    }

    void checkMyToken() {
        if (prefsHelper.getUserToken() != null) {
            getToken().subscribeWith(getObserver());
        } else {
            goToNextScreen();
        }
    }

    private Flowable<LoginResponse> getToken() {
        return NetworkClient.getRetrofit().create(NetworkInterface.class)
                .auth("Bearer " + prefsHelper.getUserToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private FlowableSubscriber<LoginResponse> getObserver() {
        return new FlowableSubscriber<LoginResponse>() {
            @Override
            public void onNext(LoginResponse loginResponse) {
                if (loginResponse != null) {
                    Log.d("Test",loginResponse.toString());
                    if (loginResponse.isError()) {
                        view.failed("Ошибка: "+loginResponse.getCode());
                        prefsHelper.setUserType(-1);
                        prefsHelper.setUserToken(null);
                    } else {
                        view.success();
                    }
                } else {
                    view.error();
                }
            }

            @Override
            public void onSubscribe(Subscription s) {

            }

            @Override
            public void onComplete() {

            }

            @Override
            public void onError(Throwable e) {
            }
        };
    }

    void goToNextScreen() {
        int type = prefsHelper.getUserType();
        view.goToMainActivity(type);

    }
}
