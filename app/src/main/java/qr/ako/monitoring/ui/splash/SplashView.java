package qr.ako.monitoring.ui.splash;

public interface SplashView {

    void goToMainActivity(int type);

    void success();

    void failed(String err);

    void error();
}
