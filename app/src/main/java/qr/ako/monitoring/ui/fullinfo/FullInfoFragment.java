package qr.ako.monitoring.ui.fullinfo;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import qr.ako.monitoring.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FullInfoFragment extends Fragment {


    public FullInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_full_info, container, false);
    }

}
