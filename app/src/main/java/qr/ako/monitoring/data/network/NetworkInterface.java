package qr.ako.monitoring.data.network;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import qr.ako.monitoring.data.network.models.CheckToken;
import qr.ako.monitoring.data.network.models.JournalDetail;
import qr.ako.monitoring.data.network.models.JournalsResponse;
import qr.ako.monitoring.data.network.models.LoginResponse;
import qr.ako.monitoring.data.network.models.MainInfo;
import qr.ako.monitoring.data.network.models.User;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface NetworkInterface {

    @POST("register")
    @FormUrlEncoded
    Flowable<LoginResponse> login(@Field("login") String login,
                                  @Field("password") String password);

    @POST("auth")
    Flowable<LoginResponse> auth(@Header("Authorization") String token);

    @POST("logout")
    Flowable<CheckToken> logout(@Header("Authorization") String token);

    @POST("api/main/info")
    Flowable<MainInfo> info(@Header("Authorization") String token);

    @POST("api/main/users")
    Flowable<List<User>> users(@Header("Authorization") String token);

    @POST("api/main/documents")
    Flowable<List<JournalDetail>> documents(@Header("Authorization") String token);

    @POST("api/main/missed")
    Flowable<List<JournalDetail>> missed(@Header("Authorization") String token);

    @POST("api/journal1/add")
    @FormUrlEncoded
    Flowable<JournalsResponse> journal1Add(@Field("number") String number, @Field("date") String date,
                                        @Field("end_date") String end_date, @Field("sender") String sender,
                                        @Field("address") String address, @Field("desc") String desc,
                                        @Field("user_id") int user_id, @Field("result") String result,
                                        @Field("limitation") int limitation, @Field("status") int status,
                                        @Field("supervisor") int supervisor, @Header("Authorization") String token);

    @POST("api/journal1/edit")
    @FormUrlEncoded
    Flowable<JournalsResponse> journal1Edit(@Field("number") String number, @Field("document_number") String document_number,
                                            @Field("date") String date, @Field("end_date") String end_date,
                                            @Field("sender") String sender, @Field("address") String address,
                                            @Field("desc") String desc, @Field("user_id") int user_id,
                                            @Field("result") String result, @Field("limitation") int limitation,
                                            @Field("status") int status, @Field("supervisor") int supervisor,
                                            @Header("Authorization") String token);

    @POST("api/journal1/delete")
    @FormUrlEncoded
    Flowable<JournalsResponse> journal1Delete(@Field("id") int id,@Header("Authorization") String token);

    @POST("api/journal1/detail")
    @FormUrlEncoded
    Flowable<JournalDetail> jounal1Detail(@Field("id") int id,@Header("Authorization") String token);

    @POST("api/journal2/add")
    @FormUrlEncoded
    Flowable<JournalsResponse> journal2Add(@Field("number") String number,@Field("date") String date,
                                       @Field("end_date") String end_date, @Field("sender") String sender,
                                       @Field("address") String address, @Field("desc") String desc,
                                       @Field("user_id") int user_id, @Field("result") String result,
                                       @Field("limitation") int limitation, @Field("status") int status,
                                       @Field("supervisor") int supervisor, @Header("Authorization") String token);

    @POST("api/journal1/edit")
    @FormUrlEncoded
    Flowable<JournalsResponse> journal2Edit(@Field("number") String number, @Field("document_number") String document_number,
                                            @Field("date") String date, @Field("end_date") String end_date,
                                            @Field("sender") String sender, @Field("address") String address,
                                            @Field("desc") String desc, @Field("user_id") int user_id,
                                            @Field("result") String result, @Field("limitation") int limitation,
                                            @Field("status") int status, @Field("supervisor") int supervisor,
                                            @Header("Authorization") String token);

    @POST("api/journal1/delete")
    @FormUrlEncoded
    Flowable<JournalsResponse> journal2Delete(@Field("id") int id, @Header("Authorization") String token);

    @POST("api/journal1/detail")
    @FormUrlEncoded
    Flowable<JournalDetail> jounal2Detail(@Field("id") int id,@Header("Authorization") String token);
}
