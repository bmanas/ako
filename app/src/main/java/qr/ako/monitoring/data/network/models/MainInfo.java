package qr.ako.monitoring.data.network.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MainInfo implements Parcelable{

    @SerializedName("executors_count")
    @Expose
    private int executorsCount;
    @SerializedName("j1_count")
    @Expose
    private int j1Count;
    @SerializedName("j1_5")
    @Expose
    private int j15;
    @SerializedName("j1_1")
    @Expose
    private int j11;
    @SerializedName("j1_missed")
    @Expose
    private int j1Missed;
    @SerializedName("j2_count")
    @Expose
    private int j2Count;
    @SerializedName("j2_5")
    @Expose
    private int j25;
    @SerializedName("j2_1")
    @Expose
    private int j21;
    @SerializedName("j2_missed")
    @Expose
    private int j2Missed;
    public final static Parcelable.Creator<MainInfo> CREATOR = new Creator<MainInfo>() {


        @SuppressWarnings({
                "unchecked"
        })
        public MainInfo createFromParcel(Parcel in) {
            return new MainInfo(in);
        }

        public MainInfo[] newArray(int size) {
            return (new MainInfo[size]);
        }

    }
            ;

    protected MainInfo(Parcel in) {
        this.executorsCount = ((int) in.readValue((int.class.getClassLoader())));
        this.j1Count = ((int) in.readValue((int.class.getClassLoader())));
        this.j15 = ((int) in.readValue((int.class.getClassLoader())));
        this.j11 = ((int) in.readValue((int.class.getClassLoader())));
        this.j1Missed = ((int) in.readValue((int.class.getClassLoader())));
        this.j2Count = ((int) in.readValue((int.class.getClassLoader())));
        this.j25 = ((int) in.readValue((int.class.getClassLoader())));
        this.j21 = ((int) in.readValue((int.class.getClassLoader())));
        this.j2Missed = ((int) in.readValue((int.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public MainInfo() {
    }

    /**
     *
     * @param j1Missed
     * @param j2Count
     * @param j1Count
     * @param j11
     * @param j2Missed
     * @param j21
     * @param j15
     * @param executorsCount
     * @param j25
     */
    public MainInfo(int executorsCount, int j1Count, int j15, int j11, int j1Missed, int j2Count, int j25, int j21, int j2Missed) {
        super();
        this.executorsCount = executorsCount;
        this.j1Count = j1Count;
        this.j15 = j15;
        this.j11 = j11;
        this.j1Missed = j1Missed;
        this.j2Count = j2Count;
        this.j25 = j25;
        this.j21 = j21;
        this.j2Missed = j2Missed;
    }

    public int getExecutorsCount() {
        return executorsCount;
    }

    public void setExecutorsCount(int executorsCount) {
        this.executorsCount = executorsCount;
    }

    public MainInfo withExecutorsCount(int executorsCount) {
        this.executorsCount = executorsCount;
        return this;
    }

    public int getJ1Count() {
        return j1Count;
    }

    public void setJ1Count(int j1Count) {
        this.j1Count = j1Count;
    }

    public MainInfo withJ1Count(int j1Count) {
        this.j1Count = j1Count;
        return this;
    }

    public int getJ15() {
        return j15;
    }

    public void setJ15(int j15) {
        this.j15 = j15;
    }

    public MainInfo withJ15(int j15) {
        this.j15 = j15;
        return this;
    }

    public int getJ11() {
        return j11;
    }

    public void setJ11(int j11) {
        this.j11 = j11;
    }

    public MainInfo withJ11(int j11) {
        this.j11 = j11;
        return this;
    }

    public int getJ1Missed() {
        return j1Missed;
    }

    public void setJ1Missed(int j1Missed) {
        this.j1Missed = j1Missed;
    }

    public MainInfo withJ1Missed(int j1Missed) {
        this.j1Missed = j1Missed;
        return this;
    }

    public int getJ2Count() {
        return j2Count;
    }

    public void setJ2Count(int j2Count) {
        this.j2Count = j2Count;
    }

    public MainInfo withJ2Count(int j2Count) {
        this.j2Count = j2Count;
        return this;
    }

    public int getJ25() {
        return j25;
    }

    public void setJ25(int j25) {
        this.j25 = j25;
    }

    public MainInfo withJ25(int j25) {
        this.j25 = j25;
        return this;
    }

    public int getJ21() {
        return j21;
    }

    public void setJ21(int j21) {
        this.j21 = j21;
    }

    public MainInfo withJ21(int j21) {
        this.j21 = j21;
        return this;
    }

    public int getJ2Missed() {
        return j2Missed;
    }

    public void setJ2Missed(int j2Missed) {
        this.j2Missed = j2Missed;
    }

    public MainInfo withJ2Missed(int j2Missed) {
        this.j2Missed = j2Missed;
        return this;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(executorsCount);
        dest.writeValue(j1Count);
        dest.writeValue(j15);
        dest.writeValue(j11);
        dest.writeValue(j1Missed);
        dest.writeValue(j2Count);
        dest.writeValue(j25);
        dest.writeValue(j21);
        dest.writeValue(j2Missed);
    }

    public int describeContents() {
        return 0;
    }

}
