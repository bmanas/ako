package qr.ako.monitoring.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefsHelper {

    private static final String FILE_NAME = "prefs";
    private static final String USER_TOKEN = "user_token";
    private static final String USER_TYPE = "user_type";
    private static final String USER_NAME = "user_name";
    private static final String USER_DESC = "user_desc";

    private SharedPreferences preferences;

    public SharedPrefsHelper(Context context) {
        preferences = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
    }

    public void setUserToken(String token) {
        preferences.edit().putString(USER_TOKEN, token).apply();
    }

    public void setUserType(int type) {
        preferences.edit().putInt(USER_TYPE, type).apply();
    }

    public int getUserType() {
        return preferences.getInt(USER_TYPE, -1);
    }

    public String getUserToken() {
        return preferences.getString(USER_TOKEN, null);
    }

    public void setUserName(String name) {
        preferences.edit().putString(USER_NAME, name).apply();
    }

    public String getUserName() {
        return preferences.getString(USER_NAME, null);
    }

    public void setUserDesc(String description) {
        preferences.edit().putString(USER_DESC, description).apply();
    }

    public String getUserDesc() {
        return preferences.getString(USER_DESC, null);
    }
}
