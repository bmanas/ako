package qr.ako.monitoring.data.network.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User implements Parcelable
{

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("j1_count")
    @Expose
    private int j1Count;
    @SerializedName("j1_5")
    @Expose
    private int j15;
    @SerializedName("j1_1")
    @Expose
    private int j11;
    @SerializedName("j1_missed")
    @Expose
    private int j1Missed;
    @SerializedName("j2_count")
    @Expose
    private int j2Count;
    @SerializedName("j2_5")
    @Expose
    private int j25;
    @SerializedName("j2_1")
    @Expose
    private int j21;
    @SerializedName("j2_missed")
    @Expose
    private int j2Missed;
    public final static Parcelable.Creator<User> CREATOR = new Creator<User>() {


        @SuppressWarnings({
                "unchecked"
        })
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        public User[] newArray(int size) {
            return (new User[size]);
        }

    }
            ;

    protected User(Parcel in) {
        this.id = ((int) in.readValue((int.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.desc = ((String) in.readValue((String.class.getClassLoader())));
        this.phone = ((String) in.readValue((String.class.getClassLoader())));
        this.j1Count = ((int) in.readValue((int.class.getClassLoader())));
        this.j15 = ((int) in.readValue((int.class.getClassLoader())));
        this.j11 = ((int) in.readValue((int.class.getClassLoader())));
        this.j1Missed = ((int) in.readValue((int.class.getClassLoader())));
        this.j2Count = ((int) in.readValue((int.class.getClassLoader())));
        this.j25 = ((int) in.readValue((int.class.getClassLoader())));
        this.j21 = ((int) in.readValue((int.class.getClassLoader())));
        this.j2Missed = ((int) in.readValue((int.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public User() {
    }

    /**
     *
     * @param j1Missed
     * @param id
     * @param j2Count
     * @param j1Count
     * @param phone
     * @param desc
     * @param name
     * @param j11
     * @param j2Missed
     * @param j21
     * @param j15
     * @param j25
     */
    public User(int id, String name, String desc, String phone, int j1Count, int j15, int j11, int j1Missed, int j2Count, int j25, int j21, int j2Missed) {
        super();
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.phone = phone;
        this.j1Count = j1Count;
        this.j15 = j15;
        this.j11 = j11;
        this.j1Missed = j1Missed;
        this.j2Count = j2Count;
        this.j25 = j25;
        this.j21 = j21;
        this.j2Missed = j2Missed;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User withId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User withName(String name) {
        this.name = name;
        return this;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public User withDesc(String desc) {
        this.desc = desc;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public User withPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public int getJ1Count() {
        return j1Count;
    }

    public void setJ1Count(int j1Count) {
        this.j1Count = j1Count;
    }

    public User withJ1Count(int j1Count) {
        this.j1Count = j1Count;
        return this;
    }

    public int getJ15() {
        return j15;
    }

    public void setJ15(int j15) {
        this.j15 = j15;
    }

    public User withJ15(int j15) {
        this.j15 = j15;
        return this;
    }

    public int getJ11() {
        return j11;
    }

    public void setJ11(int j11) {
        this.j11 = j11;
    }

    public User withJ11(int j11) {
        this.j11 = j11;
        return this;
    }

    public int getJ1Missed() {
        return j1Missed;
    }

    public void setJ1Missed(int j1Missed) {
        this.j1Missed = j1Missed;
    }

    public User withJ1Missed(int j1Missed) {
        this.j1Missed = j1Missed;
        return this;
    }

    public int getJ2Count() {
        return j2Count;
    }

    public void setJ2Count(int j2Count) {
        this.j2Count = j2Count;
    }

    public User withJ2Count(int j2Count) {
        this.j2Count = j2Count;
        return this;
    }

    public int getJ25() {
        return j25;
    }

    public void setJ25(int j25) {
        this.j25 = j25;
    }

    public User withJ25(int j25) {
        this.j25 = j25;
        return this;
    }

    public int getJ21() {
        return j21;
    }

    public void setJ21(int j21) {
        this.j21 = j21;
    }

    public User withJ21(int j21) {
        this.j21 = j21;
        return this;
    }

    public int getJ2Missed() {
        return j2Missed;
    }

    public void setJ2Missed(int j2Missed) {
        this.j2Missed = j2Missed;
    }

    public User withJ2Missed(int j2Missed) {
        this.j2Missed = j2Missed;
        return this;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(name);
        dest.writeValue(desc);
        dest.writeValue(phone);
        dest.writeValue(j1Count);
        dest.writeValue(j15);
        dest.writeValue(j11);
        dest.writeValue(j1Missed);
        dest.writeValue(j2Count);
        dest.writeValue(j25);
        dest.writeValue(j21);
        dest.writeValue(j2Missed);
    }

    public int describeContents() {
        return 0;
    }

}
