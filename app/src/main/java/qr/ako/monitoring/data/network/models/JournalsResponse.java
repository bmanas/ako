package qr.ako.monitoring.data.network.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JournalsResponse implements Parcelable
{

    @SerializedName("error")
    @Expose
    private boolean error;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("msg")
    @Expose
    private String msg;
    public final static Parcelable.Creator<JournalsResponse> CREATOR = new Creator<JournalsResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public JournalsResponse createFromParcel(Parcel in) {
            return new JournalsResponse(in);
        }

        public JournalsResponse[] newArray(int size) {
            return (new JournalsResponse[size]);
        }

    }
            ;

    protected JournalsResponse(Parcel in) {
        this.error = ((boolean) in.readValue((boolean.class.getClassLoader())));
        this.code = ((String) in.readValue((String.class.getClassLoader())));
        this.msg = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public JournalsResponse() {
    }

    /**
     *
     * @param error
     * @param code
     * @param msg
     */
    public JournalsResponse(boolean error, String code, String msg) {
        super();
        this.error = error;
        this.code = code;
        this.msg = msg;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public JournalsResponse withError(boolean error) {
        this.error = error;
        return this;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public JournalsResponse withCode(String code) {
        this.code = code;
        return this;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public JournalsResponse withMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(code);
        dest.writeValue(msg);
    }

    public int describeContents() {
        return 0;
    }

}