package qr.ako.monitoring.data.network.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckToken implements Parcelable
{

    @SerializedName("error")
    @Expose
    private boolean error;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("type")
    @Expose
    private int type;
    public final static Parcelable.Creator<CheckToken> CREATOR = new Creator<CheckToken>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CheckToken createFromParcel(Parcel in) {
            return new CheckToken(in);
        }

        public CheckToken[] newArray(int size) {
            return (new CheckToken[size]);
        }

    }
            ;

    protected CheckToken(Parcel in) {
        this.error = ((boolean) in.readValue((boolean.class.getClassLoader())));
        this.code = ((String) in.readValue((String.class.getClassLoader())));
        this.type = ((int) in.readValue((int.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public CheckToken() {
    }

    /**
     *
     * @param error
     * @param code
     * @param type
     */
    public CheckToken(boolean error, String code, int type) {
        super();
        this.error = error;
        this.code = code;
        this.type = type;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public CheckToken withError(boolean error) {
        this.error = error;
        return this;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public CheckToken withCode(String code) {
        this.code = code;
        return this;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public CheckToken withType(int type) {
        this.type = type;
        return this;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(code);
        dest.writeValue(type);
    }

    public int describeContents() {
        return 0;
    }

}