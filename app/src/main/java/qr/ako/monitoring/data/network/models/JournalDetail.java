package qr.ako.monitoring.data.network.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JournalDetail implements Parcelable{

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("limitation")
    @Expose
    private int limitation;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("sender")
    @Expose
    private String sender;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("executor")
    @Expose
    private String executor;
    @SerializedName("result")
    @Expose
    private Object result;
    @SerializedName("supervisor")
    @Expose
    private int supervisor;
    @SerializedName("type")
    @Expose
    private int type;
    @SerializedName("d_number")
    @Expose
    private String dNumber;
    @SerializedName("comm")
    @Expose
    private String comm;
    public final static Parcelable.Creator<JournalDetail> CREATOR = new Creator<JournalDetail>() {


        @SuppressWarnings({
                "unchecked"
        })
        public JournalDetail createFromParcel(Parcel in) {
            return new JournalDetail(in);
        }

        public JournalDetail[] newArray(int size) {
            return (new JournalDetail[size]);
        }

    }
            ;

    protected JournalDetail(Parcel in) {
        this.id = ((int) in.readValue((int.class.getClassLoader())));
        this.number = ((String) in.readValue((String.class.getClassLoader())));
        this.date = ((String) in.readValue((String.class.getClassLoader())));
        this.limitation = ((int) in.readValue((int.class.getClassLoader())));
        this.endDate = ((String) in.readValue((String.class.getClassLoader())));
        this.sender = ((String) in.readValue((String.class.getClassLoader())));
        this.address = ((String) in.readValue((String.class.getClassLoader())));
        this.desc = ((String) in.readValue((String.class.getClassLoader())));
        this.executor = ((String) in.readValue((String.class.getClassLoader())));
        this.result = ((Object) in.readValue((Object.class.getClassLoader())));
        this.supervisor = ((int) in.readValue((int.class.getClassLoader())));
        this.type = ((int) in.readValue((int.class.getClassLoader())));
        this.dNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.comm = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public JournalDetail() {
    }

    /**
     *
     * @param comm
     * @param result
     * @param desc
     * @param dNumber
     * @param executor
     * @param number
     * @param endDate
     * @param limitation
     * @param date
     * @param type
     * @param id
     * @param sender
     * @param address
     * @param supervisor
     */
    public JournalDetail(int id, String number, String date, int limitation, String endDate, String sender, String address, String desc, String executor, Object result, int supervisor, int type, String dNumber, String comm) {
        super();
        this.id = id;
        this.number = number;
        this.date = date;
        this.limitation = limitation;
        this.endDate = endDate;
        this.sender = sender;
        this.address = address;
        this.desc = desc;
        this.executor = executor;
        this.result = result;
        this.supervisor = supervisor;
        this.type = type;
        this.dNumber = dNumber;
        this.comm = comm;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public JournalDetail withId(int id) {
        this.id = id;
        return this;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public JournalDetail withNumber(String number) {
        this.number = number;
        return this;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public JournalDetail withDate(String date) {
        this.date = date;
        return this;
    }

    public int getLimitation() {
        return limitation;
    }

    public void setLimitation(int limitation) {
        this.limitation = limitation;
    }

    public JournalDetail withLimitation(int limitation) {
        this.limitation = limitation;
        return this;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public JournalDetail withEndDate(String endDate) {
        this.endDate = endDate;
        return this;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public JournalDetail withSender(String sender) {
        this.sender = sender;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public JournalDetail withAddress(String address) {
        this.address = address;
        return this;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public JournalDetail withDesc(String desc) {
        this.desc = desc;
        return this;
    }

    public String getExecutor() {
        return executor;
    }

    public void setExecutor(String executor) {
        this.executor = executor;
    }

    public JournalDetail withExecutor(String executor) {
        this.executor = executor;
        return this;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public JournalDetail withResult(Object result) {
        this.result = result;
        return this;
    }

    public int getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(int supervisor) {
        this.supervisor = supervisor;
    }

    public JournalDetail withSupervisor(int supervisor) {
        this.supervisor = supervisor;
        return this;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public JournalDetail withType(int type) {
        this.type = type;
        return this;
    }

    public String getDNumber() {
        return dNumber;
    }

    public void setDNumber(String dNumber) {
        this.dNumber = dNumber;
    }

    public JournalDetail withDNumber(String dNumber) {
        this.dNumber = dNumber;
        return this;
    }

    public String getComm() {
        return comm;
    }

    public void setComm(String comm) {
        this.comm = comm;
    }

    public JournalDetail withComm(String comm) {
        this.comm = comm;
        return this;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(number);
        dest.writeValue(date);
        dest.writeValue(limitation);
        dest.writeValue(endDate);
        dest.writeValue(sender);
        dest.writeValue(address);
        dest.writeValue(desc);
        dest.writeValue(executor);
        dest.writeValue(result);
        dest.writeValue(supervisor);
        dest.writeValue(type);
        dest.writeValue(dNumber);
        dest.writeValue(comm);
    }

    public int describeContents() {
        return 0;
    }

}